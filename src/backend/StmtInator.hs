module StmtInator where
import AbsLatte
import StateInator
import ExprInator
import Control.Monad.Reader
import qualified Data.Map as Map
import Control.Monad.State
import Control.Monad.Except

transStmt :: Stmt -> MonadT (Env, Instr)
transStmt (Empty _) = do
    env <- ask
    return (env, EmptyLine)  
transStmt (BStmt _ (Block _ stmts)) = do
    env <- ask
    (_, code) <- foldM transStmts (env, EmptyLine) stmts
    return (env, code)
    where
        transStmts :: (Env, Instr) -> Stmt -> MonadT (Env, Instr)
        transStmts (accEnv, accCode) stmt = do
            (nextEnv, nextCode) <- local (const accEnv) $ transStmt stmt
            return (nextEnv, AsmBlock [accCode, nextCode])
transStmt (Decl _ t items) = do
    env <- ask
    foldM transDecls (env, EmptyLine) items
    where
        transDecl :: Item -> MonadT (Env, Instr)
        transDecl item = do
            loc <- getNewLocal
            (ident, code) <- case item of
                (NoInit _ idnt) -> return (idnt, MOV (defaultValueType t) (Mem loc))
                (Init _ idnt expr) -> do
                    initCode <- transExpr expr
                    return (idnt, AsmBlock [initCode, MOV (Reg EAX) (Mem loc)])
            env <- ask
            return (env {
                 varToMem = Map.insert ident loc (varToMem env),
                 varToType = Map.insert ident t (varToType env)
            }, code)
        transDecls :: (Env, Instr) -> Item -> MonadT (Env, Instr)
        transDecls (accEnv, accCode) item = do
            (nextEnv, nextCode) <- local (const accEnv) $ transDecl item
            return (nextEnv, AsmBlock [accCode, nextCode])
transStmt (Ass _ expr1 expr2) = do
    leftCode <- transLeftValue expr1
    exprCode <- transExpr expr2
    env <- ask
    let code = AsmBlock [
            leftCode,
            PUSH (Reg EAX),
            exprCode,
            POP (Reg EDX),
            MOV (Reg EAX) (Addr EDX)]
    return (env, code)
transStmt (Incr _ expr) =  do
    leftCode <- transLeftValue expr
    env <- ask
    let code = AsmBlock [
            leftCode,
            UnIns INC (Addr EAX)]
    return (env, code)
transStmt (Decr _ expr) =do
    leftCode <- transLeftValue expr
    env <- ask
    let code = AsmBlock [
            leftCode,
            UnIns DEC (Addr EAX)]
    return (env, code)
transStmt (Ret _ expr) = do
    exprCode <- transExpr expr
    env <- ask
    lab <- gets retFunLabel
    let code = AsmBlock [
            exprCode,
            Jump JMP lab] 
    return (env, code)
transStmt (VRet _) = do
    env <- ask
    lab <- gets retFunLabel
    return (env, Jump JMP lab)
transStmt (Cond _ expr stmt) = do
    trueLabel <- getNewLabel
    exitLabel <- getNewLabel
    exprCode <- transCond expr trueLabel exitLabel
    (_, trueCode) <- transStmt (BStmt Nothing (Block Nothing [stmt]))
    let code = AsmBlock [
            exprCode, 
            Lab trueLabel,
            trueCode,
            Lab exitLabel]
    env <- ask
    return (env, code)
transStmt (CondElse _ expr stmt1 stmt2) =  do
    trueLabel <- getNewLabel
    falseLabel <- getNewLabel
    exitLabel <- getNewLabel
    exprCode <- transCond expr trueLabel falseLabel
    (_, trueCode)  <- transStmt (BStmt Nothing (Block Nothing [stmt1]))
    (_, falseCode) <- transStmt (BStmt Nothing (Block Nothing [stmt2]))
    let code = AsmBlock [
            exprCode,
            Lab trueLabel,
            trueCode,
            Jump JMP exitLabel,
            Lab falseLabel,
            falseCode,
            Lab exitLabel]
    env <- ask
    return (env, code)
transStmt (While _ expr stmt) = do
    condLabel <- getNewLabel
    loopLabel <- getNewLabel
    exitLabel <- getNewLabel
    exprCode <- transCond expr loopLabel exitLabel
    (_, bodyCode)  <- transStmt (BStmt Nothing (Block Nothing [stmt]))
    let code = AsmBlock [
            Jump JMP condLabel,
            Lab loopLabel,
            bodyCode,
            Lab condLabel,
            exprCode,
            Lab exitLabel]
    env <- ask
    return (env, code)
transStmt (SExp _ expr) = do
    env <- ask
    code <- transExpr expr
    return (env, code)
transStmt (For p type_ ident expr stmt) = do
    let iter = Ident "_iter"
    let pointer = Ident "_pointer"
    t <- getExprType expr -- potrzebne?
    let forStmt = BStmt p $ Block p [
            Decl p (Int p) [Init p iter (ELitInt p 0)],
            Decl p t [Init p pointer expr],
            While p
                (ERel p (EVar p iter) (LTH p) (EAttrAcc p (EVar p pointer) (Ident "length")))
                (BStmt p $ Block p [
                    Decl p t [Init p ident (EArrAcc p (EVar p pointer) (EVar p iter))],
                    stmt,
                    Incr p (EVar p iter)
                ])
            ]
    transStmt forStmt