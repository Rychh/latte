module StateInator where
import AbsLatte
import Control.Monad.State
import Control.Monad.Reader
import qualified Data.Map as Map
import Control.Monad.Except
import Data.List (intercalate)

type MonadT a = ReaderT Env (ExceptT String (StateT Store IO)) a

runMonadT :: MonadT a -> IO (Either String a, Store)
runMonadT e = runStateT (runExceptT (runReaderT e initialEnv)) initialState

data Env = Env {
  varToMem :: Map.Map Ident Memory,
  varToType :: Map.Map Ident Type,
  insideClass :: Maybe Ident
}

data Store = Store {
    nextJmpLabel :: Int,
    instr :: Instr,
    strToLab :: Map.Map String Label,
    locals :: Int,
    retFunLabel :: Label,
    funToType :: Map.Map Ident Type,
    classToDef :: Map.Map Ident ClassDef,
    classToCEnv :: Map.Map Ident ClassEnv
}

data ClassDef = ClassDef {
  methods :: Map.Map Ident Type,
  attrs :: Map.Map Ident Type,
  extends :: Maybe Ident
} deriving Show

data ClassEnv = ClassEnv {
  attrToMem :: Map.Map Ident Memory,
  attrToType :: Map.Map Ident Type,
  methToCMeth :: Map.Map Ident ClassMethod,
  methToCMethList :: [(Ident, ClassMethod)]
} deriving Show

data ClassMethod = ClassMethod {
  retType :: Type,
  methId :: Int,
  clsName :: Ident
} deriving Show

initialEnv :: Env
initialEnv = Env {
  varToMem = Map.empty,
  varToType = Map.empty,
  insideClass = Nothing
  }

initialState :: Store
initialState = Store {
    nextJmpLabel = 0,
    instr = EmptyLine,
    strToLab = Map.empty,
    locals = 0,
    retFunLabel = JmpL (-1),
    funToType = Map.fromList [
       (Ident "printInt", Void Nothing ),
       (Ident "printString", Void Nothing ),
       (Ident "readInt", Int Nothing ),
       (Ident "readString", Str Nothing ),
       (Ident "_concatStrings" , Str Nothing ),
       (Ident "_equalStrings", Bool Nothing )],
    classToDef = Map.empty,
    classToCEnv = Map.empty 
}


data Register = EAX | ECX  | EDX | EBP | ESP
instance Show Register where
  show EAX = "%eax"
  show ECX = "%ecx"
  show EDX = "%edx"
  show EBP = "%ebp"
  show ESP = "%esp"

data JOp = JL | JLE | JG | JGE | JE | JNE | JMP
instance Show JOp where
  show JL  = "jl      "
  show JLE = "jle     "
  show JG  = "jg      "
  show JGE = "jge     "
  show JE  = "je      "
  show JNE = "jne     "
  show JMP = "jmp     "

data Label = JmpL Int 
  | FunL Ident 
  | StrL Int String 
  | RetFunL Ident 
  | RetMethL Ident Ident 
  | MethL Ident Ident
  | ClsL Ident
instance Show Label where
  show (JmpL int) = ".L" ++ show int
  show (FunL (Ident s)) =  s
  show (RetFunL (Ident s)) =  s ++ "_return"
  show (StrL int string) = ".LC" ++ show int
  show (RetMethL (Ident className) (Ident methodName)) = className ++ "_" ++ methodName ++ "_return"
  show (MethL (Ident className) (Ident methodName)) = className ++ "_" ++ methodName
  show (ClsL (Ident className)) = className

data BinOp = ADD | SUB | MUL | DIV | XOR | CMP
instance Show BinOp where
  show ADD = "addl    "
  show SUB = "subl    "
  show DIV = "idiv    "
  show MUL = "imul    "
  show XOR = "xor     "
  show CMP = "cmp     "

data UnOp = NEG | INC | DEC
instance Show UnOp where
  show NEG = "neg     "
  show INC = "incl    "
  show DEC = "decl    "

data Memory = Local Int | Param Int | Attribute Int
instance Show Memory where
  show (Local int) = show (-int * dword) ++ "(" ++ show EBP ++ ")"
  show (Param int) = show ((int + 1) * dword) ++ "(" ++ show EBP ++ ")"
  show (Attribute int) = show (int * dword) ++ "(" ++ show EAX ++ ")"

data Operand = Reg Register
  | Lit Int
  | StrLit Label
  | ClsLit Label
  | Mem Memory
  | Addr Register
  | AddrArrElem Register Register Int
  | AttrAddr Memory Register
  | MethAddr Int Register
instance Show Operand where
  show (Reg r) = show r
  show (Lit l) = '$' : show l
  show (StrLit l) = '$' : show l
  show (ClsLit l) = '$' : show l
  show (Mem m) = show m
  show (Addr r) = "(" ++ show r ++ ")"
  show (AddrArrElem r1 r2 i) = "(" ++ show r1 ++ "," ++ show r2 ++ "," ++ show i ++ ")"
  show (AttrAddr (Attribute i) r) =  show (i * dword) ++ "(" ++ show r ++ ")"
  show AttrAddr {} = "Something goes wrong :<"
  show (MethAddr i r) = "*" ++ show (i * dword) ++ "(" ++ show r ++ ")"

data Instr = AsmBlock [Instr] 
  | MOV Operand Operand
  | LEA Operand Operand
  | PUSH Operand
  | POP Operand
  | Jump JOp Label
  | UnIns UnOp Operand
  | BinIns BinOp Operand Operand
  | CDQ
  | RET
  | Lab Label
  | EmptyLine
  | CALL Ident
  | CallMethod Operand
  | StackAlloc Int
  | DataSection
  | TextSection
  | Intro
  | MTable [Label]
instance Show Instr where
  show (AsmBlock instrs) = intercalate "\n" [show i | i <- instrs]
  show (MOV op1 op2) = "\t" ++ "movl    " ++ show op1 ++ ", " ++ show op2
  show (LEA op1 op2) = "\t" ++ "leal    " ++ show op1 ++ ", " ++ show op2
  show (PUSH op) = "\t" ++ "pushl   " ++ show op
  show (POP  op) = "\t" ++ "popl    " ++ show op
  show (Jump jmp op) ="\t" ++ show jmp ++ show op
  show (UnIns un op) = "\t" ++ show un ++ show op
  show (BinIns bin op1 op2) ="\t" ++ show bin ++ show op1 ++ ", " ++ show op2
  show CDQ = "\t" ++ "cdq     "
  show RET = "\t" ++ "ret\n"
  show (Lab l@(StrL _ str)) = show l ++ ":\t.asciz " ++ show str
  show (Lab l) = show l ++ ":"
  show EmptyLine = ""
  show (CALL (Ident string)) = "\t" ++"call    " ++ string
  show (StackAlloc 0) = ""
  show (StackAlloc int) = show $ BinIns SUB (Lit (int * dword)) (Reg ESP)
  show Intro = ".globl main"
  show DataSection = ".data"
  show TextSection = "\n.text"
  show (CallMethod m@MethAddr{}) =  "\tcall    " ++ show m
  show (CallMethod _) = "Something goes wrong :<"
  show (MTable lbls) = ".int     " ++ intercalate ", " (map show lbls)

getFunRetType :: Ident -> MonadT Type
getFunRetType ident = do
  f2t <- gets funToType
  case Map.lookup ident f2t of
    Nothing -> throwError $ impossibleError ++ "missing fun type."
    Just t -> return t

getVarType :: Ident -> MonadT Type
getVarType ident = do
  v2t <- asks varToType
  case Map.lookup ident v2t of
    Nothing -> throwError $ impossibleError ++ "missing var type."
    Just t -> return t

getVarMem :: Ident -> MonadT Memory
getVarMem ident = do
  v2m <- asks varToMem
  case Map.lookup ident v2m of
    Nothing -> throwError $ impossibleError ++ "\n" ++ show v2m ++ "\n" ++ show ident ++ "\n" ++ "missing var memory."
    Just mem -> return mem

dword :: Int
dword = 4

nullPtr :: Operand
nullPtr = Lit 0

trueLit,falseLit :: Operand
trueLit = Lit 1
falseLit = Lit 0

defaultValueType :: Type -> Operand
defaultValueType (Int _) = Lit 0
defaultValueType (Bool _) = falseLit
defaultValueType _ = nullPtr

getNewLabel :: MonadT Label
getNewLabel = do
  njl <- gets nextJmpLabel
  let lab = JmpL njl
  modify (\st -> st { nextJmpLabel = njl + 1 })
  return lab

getNewLocal :: MonadT Memory
getNewLocal = do
  l <- gets locals
  let loc = Local (l + 1)
  modify (\st -> st { locals = l + 1 })
  return loc

getStrLabel :: String -> MonadT Label
getStrLabel str = do
  s2l <- gets strToLab
  case Map.lookup str s2l of
      Nothing -> do
          let lab = StrL (Map.size s2l) str
          modify (\st -> st { strToLab = Map.insert str lab s2l })
          return lab
      Just lab -> return lab

getJOp :: RelOp -> JOp
getJOp op = case op of
    LTH _ -> JL
    LE  _ -> JLE
    GTH _ -> JG
    GE _ -> JGE
    EQU _ -> JE
    NE _ -> JNE

impossibleError,undefinedError :: String
impossibleError = "\ESC[31m[Translate ERROR]\ESC[0m Something went wrong. You shouldn't see this message. This error is related to "
undefinedError = "\ESC[31m[ERROR]\ESC[0m Nie do implenetowałeś:"