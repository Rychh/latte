module TransInator where

import StateInator
import StmtInator
import AbsLatte
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Except
import qualified Data.Map as Map
import Data.List (nubBy)


translate :: Program ->  IO(Either String String)
translate program =  do
  (check, store) <- runMonadT (transProgram program)
  case check of
    Left err -> return $ Left err
    Right _ -> return $ Right (show $ instr store)

transProgram :: Program -> MonadT ()
transProgram (Program _ dcls) = do
    saveTopDefs dcls
    classmethods <- createCEnvs
    instrs <- mapM transTopDef dcls
    strs <- gets strToLab
    let strLabels = map Lab $ Map.elems strs
    let code = AsmBlock [
            Intro,
            DataSection,
            classmethods,
            AsmBlock strLabels,
            TextSection,
            AsmBlock instrs]
    modify (\store -> (store {instr = code })) -- tylko w tym miejscu dodaje instr

transTopDef :: TopDef -> MonadT Instr
transTopDef (FnDef _ typ ident args block) = do
    modify (\st -> st {
        locals = 0,
        retFunLabel = RetFunL ident})
    (_, code) <- local (\env -> env {
        varToMem = Map.fromList $ genVarToMem args 1,
        varToType = Map.fromList $ genVarToType args,
        insideClass = Nothing}) (transStmt (BStmt Nothing block))
    local <- gets locals
    return $ AsmBlock [
        Lab (FunL ident),
        PUSH (Reg EBP),
        MOV (Reg ESP) (Reg EBP),
        StackAlloc local,
        code,
        Lab (RetFunL ident),
        MOV (Reg EBP) (Reg ESP),
        POP (Reg EBP),
        RET]
transTopDef (ClDef _ className classext clmembers) = do
    arr <- mapM transMethod clmembers
    return $ AsmBlock arr
    where
      transMethod :: ClMember -> MonadT Instr
      transMethod Attr{} = return EmptyLine
      transMethod (Meth _ retType methName args block) = do
        c2e <- gets classToCEnv
        case Map.lookup className c2e of
            Nothing -> throwError $ impossibleError ++ "translate method"
            Just cenv -> do
                let label = RetMethL
                modify (\st -> st {
                    locals = 0,
                    retFunLabel = RetMethL className methName})
                (_, code) <- local (\env -> env {
                    varToMem = Map.union (attrToMem cenv) $ Map.fromList ((Ident "self", Param 1):genVarToMem args 2),
                    varToType = Map.union (attrToType cenv) $ Map.fromList ((Ident "self", Cls Nothing className):genVarToType args),
                    insideClass = Nothing}) (transStmt (BStmt Nothing block))
                local <- gets locals
                return $ AsmBlock [
                    Lab (MethL className methName),
                    PUSH (Reg EBP),
                    MOV (Reg ESP) (Reg EBP),
                    StackAlloc local,
                    code,
                    Lab (RetMethL className methName),
                    MOV (Reg EBP) (Reg ESP),
                    POP (Reg EBP),
                    RET]
genVarToMem :: [Arg] -> Int -> [(Ident, Memory)]
genVarToMem [] _ = []
genVarToMem ((Arg _ _ ident):xs) i = (ident, Param i) : genVarToMem xs (i+1)
genVarToType :: [Arg] -> [(Ident, Type)]
genVarToType [] = []
genVarToType ((Arg _ typ ident):xs) = (ident, typ) : genVarToType xs

saveTopDefs :: [TopDef] -> MonadT ()
saveTopDefs [] =  return ()
saveTopDefs ((FnDef pos retTyp ident args _):topDefs) = do
    modify (\st -> st {funToType = Map.insert ident retTyp (funToType st)})
    saveTopDefs topDefs
saveTopDefs  ((ClDef pos ident classext clmembers):topDefs) = do
    let (mList, aList) = foldr getClassMember ([],[]) clmembers
    let ext = case classext of
          NoExt _ -> Nothing
          Ext _ i -> Just i
    let cDef = ClassDef {
        methods = Map.fromList mList,
        attrs = Map.fromList aList,
        extends = ext
    }
    modify (\st -> st {classToDef = Map.insert ident cDef (classToDef st)})
    saveTopDefs topDefs
  where
    getClassMember :: ClMember -> ([(Ident, Type)],[(Ident, Type)]) -> ([(Ident, Type)],[(Ident, Type)])
    getClassMember (Meth pos retTyp ident args _ )  (methAcc, attrAcc) =
       ((ident, Fun Nothing retTyp (map getArgType args)) : methAcc, attrAcc)
    getClassMember (Attr _ type_ ident) (methAcc, attrAcc) =
       (methAcc, (ident, type_):attrAcc)
    getArgType :: Arg -> Type
    getArgType (Arg _ t _) = t

createCEnvs :: MonadT Instr
createCEnvs = do
    cDefs <- gets classToDef
    mapM_ createCEnv $ Map.toList cDefs
    c2e <- gets classToCEnv
    return $ AsmBlock (map getMethodsLabels  $ Map.toList c2e)
    where
        getMethodsLabels :: (Ident, ClassEnv) -> Instr
        getMethodsLabels (cls, cenv) = if null (methToCMethList cenv)
            then EmptyLine
            else AsmBlock [
                Lab (FunL cls),
                MTable (map getLabel (methToCMethList cenv))]
        getLabel :: (Ident, ClassMethod) -> Label
        getLabel (methName, cmeth) = MethL (clsName cmeth) methName


createCEnv :: (Ident, ClassDef) -> MonadT ()
createCEnv (cls, cDef)= do
    (attrs, meths) <- getAllMembers (Just cls)
    let attrsMem = zipWith (\(k, _) i -> (k, Attribute i)) (reverse attrs) [1 ..]
    let rmeths = reverse meths
    let order = zipWith 
            (\(x,_)  i -> (x,i)) 
            (nubBy (\(m1, _) (m2, _) -> m1 == m2) rmeths)
            [0 ..]
    cmeths <- mapM (orderedMethods (Map.fromList rmeths)) order
    let cEnv = ClassEnv {
        attrToMem = Map.fromList attrsMem,
        attrToType = Map.fromList attrs,
        methToCMeth = Map.fromList cmeths, -- todo check this
        methToCMethList = cmeths
    }
    modify (\st -> st {classToCEnv = Map.insert cls cEnv (classToCEnv st)})
    where
        orderedMethods :: Map.Map Ident (Type, Ident)-> (Ident, Int) -> MonadT (Ident, ClassMethod)
        orderedMethods m (methName, methId) = case Map.lookup methName m of
            Nothing -> throwError $ impossibleError ++ " ordering methods."
            Just (retType, clsName) -> return (methName,
                ClassMethod {
                    retType = retType,
                    methId = methId,
                    clsName = clsName})
        toClassMeth :: (Ident, Type, Ident) -> Int -> (Ident, ClassMethod)
        toClassMeth (methName, retType, clsName) methId = (methName,
            ClassMethod {
                retType = retType,
                methId = methId,
                clsName = clsName})

getAllMembers :: Maybe Ident -> MonadT ([(Ident, Type)], [(Ident, (Type, Ident))])
getAllMembers Nothing = return ([], [])
getAllMembers (Just cls) = do
    c2d <- gets classToDef
    case Map.lookup cls c2d of
        Nothing -> throwError $ impossibleError ++ " translation left value."
        Just cDef -> do
            (pattrs, pmeths) <- getAllMembers (extends cDef)
            return (
                reverse (Map.toList (attrs cDef)) ++ pattrs,
                map (\(i, t) -> (i, (t, cls))) (reverse $ Map.toList (methods cDef)) ++ pmeths)