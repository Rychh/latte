module ExprInator where
import AbsLatte
import StateInator
import Control.Monad.Except
import qualified Data.Map as Map
import Control.Monad.State
import Control.Monad.Reader

transLeftValue :: Expr -> MonadT Instr
transLeftValue (EVar _ ident) = do
    loc <- getVarMem ident
    case loc of
        Attribute{} -> return $ AsmBlock [
            MOV (Mem (Param 1)) (Reg EAX),
            LEA (Mem loc) (Reg EDX),
            MOV (Reg EDX) (Reg EAX)]
        _ -> return $ LEA (Mem loc) (Reg EAX)
transLeftValue (EApp p ident exprs) = do
    insideCls <- asks insideClass
    c2e <- gets classToCEnv
    case insideCls of
        Nothing -> normalnTransEApp
        Just cls -> case Map.lookup cls c2e of
            Nothing -> throwError $ impossibleError ++ " classToCenv."
            Just cenv -> case Map.lookup ident (methToCMeth cenv) of
                Nothing -> normalnTransEApp
                Just _ -> transLeftValue (EMethCall p (EVar p (Ident "self")) ident exprs)
    where
        normalnTransEApp :: MonadT Instr
        normalnTransEApp = do
            exprsCode <- mapM transExpr exprs
            return $ AsmBlock [
                AsmBlock $ addPushEAX $ reverse exprsCode,
                CALL ident,
                BinIns ADD (Lit (dword * length exprs)) (Reg ESP)]
        addPushEAX :: [Instr] -> [Instr]
        addPushEAX [] = []
        addPushEAX (x:xs) = x : PUSH (Reg EAX) : addPushEAX xs
transLeftValue (EAttrAcc _ expr ident) = do
    accCode <- transLeftValue expr
    mem <- getCEnvAttrMem expr ident
    let assign = AsmBlock [
            MOV (Addr EAX) (Reg EDX),
            LEA (AttrAddr mem EDX) (Reg EAX)]
    let access = LEA (AttrAddr mem EDX) (Reg EAX)
    let end = case expr of
            EApp{} -> access
            EMethCall{} -> access
            ENewArr{} -> access
            ENewCls{} -> access
            _ -> assign
    return $ AsmBlock [accCode, end]
transLeftValue (EArrAcc _ exprAcc exprIndex) = do
    indexCode <- transExpr exprIndex
    accCode <- transLeftValue exprAcc
    let assign = AsmBlock [
            POP (Reg EDX),
            UnIns INC (Reg EDX),
            MOV (Addr EAX) (Reg EAX),
            LEA (AddrArrElem EAX EDX dword) (Reg EAX)]
    let access = AsmBlock [
            POP (Reg EDX),
            UnIns INC (Reg EDX),
            LEA (AddrArrElem EAX EDX dword) (Reg EAX)]
    let end = case exprAcc of
            EApp{} -> access
            EMethCall{} -> access
            ENewArr{} -> access
            ENewCls{} -> access
            _ -> assign
    return $ AsmBlock [
        indexCode,
        PUSH (Reg EAX),
        accCode,
        end
        ]
transLeftValue (EMethCall _ expr ident exprs) = do
    cmeth <- getClassMethod expr ident
    accCode <- transExpr expr
    exprsCode <- mapM transExpr exprs
    return $ AsmBlock [
        AsmBlock $ addPushEAX $ reverse exprsCode,
        accCode,
        PUSH (Reg EAX),
        MOV (Addr EAX) (Reg EAX),
        CallMethod (MethAddr (methId cmeth) EAX),
        BinIns ADD (Lit (dword * (1 + length exprs))) (Reg ESP)]
    where
        addPushEAX :: [Instr] -> [Instr]
        addPushEAX [] = []
        addPushEAX (x:xs) = x : PUSH (Reg EAX) : addPushEAX xs
transLeftValue (ENewArr _ type_ expr) = do
    sizeCode <- transExpr expr
    return $ AsmBlock [
        sizeCode,
        PUSH (Reg EAX),
        UnIns INC (Reg EAX),
        PUSH (Lit dword),
        PUSH (Reg EAX),
        CALL (Ident "calloc"),
        BinIns ADD (Lit (2 * dword)) (Reg ESP),
        POP (Reg EDX),
        MOV (Reg EDX) (Addr EAX)]
transLeftValue e@(ENewCls _ (Cls _ ident)) = do
    cenv <- getCEnv e
    let size = 1 + Map.size (attrToType cenv)
    let mtable = if null (methToCMeth cenv)
        then EmptyLine
        else MOV (ClsLit (ClsL ident)) (Addr EAX)
    return $ AsmBlock [
        PUSH (Lit dword),
        PUSH (Lit size),
        CALL (Ident "calloc"),
        BinIns ADD (Lit (2 * dword)) (Reg ESP),
        mtable]
transLeftValue _ = throwError $ impossibleError ++ " translation left value."

transExpr :: Expr -> MonadT Instr
transExpr (EVar _ ident) = do
    loc <- getVarMem ident
    case loc of
        Attribute{} -> return $ AsmBlock [
            MOV (Mem (Param 1)) (Reg EAX),
            MOV (Mem loc) (Reg EAX)]
        _ -> return $ MOV (Mem loc) (Reg EAX)
transExpr (ELitInt _ integer) =  return $ MOV (Lit $ fromIntegral integer) (Reg EAX)
transExpr (ELitTrue _) = return $ MOV trueLit (Reg EAX)
transExpr (ELitFalse _) = return $ MOV falseLit (Reg EAX)
transExpr e@EApp {} = transLeftValue e
transExpr (EString _ string) = do
    lab <- getStrLabel string
    return $ MOV (StrLit lab) (Reg EAX)
transExpr (Neg _ expr) = do
    code <- transExpr expr
    return $ AsmBlock [code, UnIns NEG (Reg EAX)]
transExpr (Not _ expr) = do
    code <- transExpr expr
    return $ AsmBlock [code, BinIns XOR (Lit 1) (Reg EAX)]
transExpr (EMul _ expr1 mulop expr2) = do
    code1 <- transExpr expr1
    code2 <- transExpr expr2
    let end = case mulop of
          Times _ -> [
              POP (Reg ECX),
              BinIns MUL (Reg EAX) (Reg ECX),
              MOV (Reg ECX) (Reg EAX)]
          Div _ -> [
              MOV (Reg EAX) (Reg ECX),
              POP (Reg EAX),
              CDQ,
              BinIns DIV (Reg ECX) (Reg EAX)]
          Mod _ -> [
              MOV (Reg EAX) (Reg ECX),
              POP (Reg EAX),
              CDQ,
              BinIns DIV (Reg ECX) (Reg EAX),
              MOV (Reg EDX) (Reg EAX)]
    return  $ AsmBlock (code1 : PUSH (Reg EAX) : code2 : end)
transExpr (EAdd _ expr1 addop expr2) = do
    t <- getExprType expr1
    case t of
        Str _ -> transExpr (EApp Nothing (Ident "_concatStrings") [expr1, expr2])
        _ -> do
            code1 <- transExpr expr1
            code2 <- transExpr expr2
            let end = case addop of
                    Plus _ -> [
                        POP (Reg ECX),
                        BinIns ADD (Reg EAX) (Reg ECX),
                        MOV (Reg ECX) (Reg EAX)]
                    Minus _ -> [
                        POP (Reg ECX),
                        BinIns SUB (Reg EAX) (Reg ECX),
                        MOV (Reg ECX) (Reg EAX)]
            return  $ AsmBlock (code1 : PUSH (Reg EAX) : code2 : end)
transExpr e@(ERel _ expr1 relop expr2) = do -- można to lepiej zrobić
    t <- getExprType expr1
    case (t, relop) of
        (Str _, EQU _) -> transExpr $ EApp Nothing (Ident "_equalStrings") [expr1, expr2]
        (Str _, NE _) -> transExpr $ Not Nothing $ EApp Nothing (Ident "_equalStrings") [expr1, expr2]
        (_, _) -> do
                trueLabel <- getNewLabel
                falseLabel <- getNewLabel
                exitLabel <- getNewLabel
                code <- transCond e trueLabel falseLabel
                return $ AsmBlock [
                    code,
                    Lab falseLabel,
                    MOV falseLit (Reg EAX),
                    Jump JMP exitLabel,
                    Lab trueLabel,
                    MOV trueLit (Reg EAX),
                    Lab exitLabel]
transExpr e@(EAnd _ expr1 expr2) = do
    trueLabel <- getNewLabel
    falseLabel <- getNewLabel
    exitLabel <- getNewLabel
    code <- transCond e trueLabel falseLabel
    return $ AsmBlock [
        code,
        Lab trueLabel,
        MOV trueLit (Reg EAX),
        Jump JMP exitLabel,
        Lab falseLabel,
        MOV falseLit (Reg EAX),
        Lab exitLabel]
transExpr e@(EOr _ expr1 expr2) = do
    trueLabel <- getNewLabel
    falseLabel <- getNewLabel
    exitLabel <- getNewLabel
    code <- transCond e trueLabel falseLabel
    return $ AsmBlock [
        code,
        Lab falseLabel,
        MOV falseLit (Reg EAX),
        Jump JMP exitLabel,
        Lab trueLabel,
        MOV trueLit (Reg EAX),
        Lab exitLabel]
transExpr e@(EAttrAcc pos expr ident) = do
    t <- getExprType expr
    case t of
        (Arr _ _) -> transExpr (EArrAcc pos expr (ELitInt pos (-1)))
        _ -> do
            accCode <- transLeftValue e
            return $ AsmBlock [
                accCode,
                MOV (Addr EAX) (Reg EAX)]
transExpr e@EArrAcc{} = do
    accCode <- transLeftValue e
    return $ AsmBlock [
        accCode,
        MOV (Addr EAX) (Reg EAX)]
transExpr e@(EMethCall _ expr ident exprs) = transLeftValue e
transExpr e@(ENewArr _ type_ expr) = transLeftValue e
transExpr e@(ENewCls _ type_) = transLeftValue e
transExpr (ECastNull _ type_) = return (MOV nullPtr (Reg EAX))

transCond :: Expr -> Label -> Label -> MonadT Instr
transCond (ERel _ expr1 relop expr2) trueLabel falseLabel = do
    t <- getExprType expr1
    case (t, relop) of
        (Str _, EQU _) -> transCond (EApp Nothing (Ident "_equalStrings") [expr1, expr2]) trueLabel falseLabel
        (Str _, NE _) -> transCond (Not Nothing $ EApp Nothing (Ident "_equalStrings") [expr1, expr2]) trueLabel falseLabel
        (_, _) -> do
            code1 <- transExpr expr1
            code2 <- transExpr expr2
            return $ AsmBlock [
                code1,
                PUSH (Reg EAX),
                code2,
                MOV (Reg EAX) (Reg ECX),
                POP (Reg EAX),
                BinIns CMP (Reg ECX) (Reg EAX),
                Jump (getJOp relop) trueLabel,
                Jump JMP falseLabel]
transCond (EAnd _ expr1 expr2) trueLabel falseLabel = do
    midLabel <- getNewLabel
    code1 <- transCond expr1 midLabel falseLabel
    code2 <- transCond expr2 trueLabel falseLabel
    return $ AsmBlock [code1, Lab midLabel, code2]
transCond (EOr _ expr1 expr2) trueLabel falseLabel = do
    midLabel <- getNewLabel
    code1 <- transCond expr1 trueLabel midLabel
    code2 <- transCond expr2 trueLabel falseLabel
    return $ AsmBlock [code1, Lab midLabel, code2]
transCond (Not _ expr) trueLabel falseLabel = transCond expr falseLabel trueLabel
transCond (ELitFalse _) _ falseLabel = return $ Jump JMP falseLabel
transCond (ELitTrue _) trueLabel _ = return $ Jump JMP trueLabel
transCond expr trueLabel falseLabel = do
    code <- transExpr expr
    return $ AsmBlock [
        code,
        BinIns CMP trueLit (Reg EAX),
        Jump JE trueLabel,
        Jump JMP falseLabel]


getExprType :: Expr -> MonadT Type
getExprType (EVar _ ident) = getVarType ident
getExprType (ELitInt _ integer) = return (Int Nothing)
getExprType (ELitTrue _) = return (Bool Nothing)
getExprType (ELitFalse _) = return (Bool Nothing)
getExprType (EApp _ ident _) = getFunRetType ident
getExprType EString{} = return (Str Nothing)
getExprType Neg{} = return (Int Nothing)
getExprType Not{} = return (Bool Nothing)
getExprType EMul {} = return (Int Nothing)
getExprType (EAdd _ expr1 _ _) = getExprType expr1
getExprType ERel {} = return (Bool Nothing)
getExprType EAnd {} = return (Bool Nothing)
getExprType EOr {} = return (Bool Nothing)
getExprType (EAttrAcc _ expr ident) = do
    t <- getExprType expr
    case (t, ident) of
        (Arr _ t, Ident "length") -> return t
        _ -> getCEnvAttrType expr ident
getExprType (EArrAcc _ expr _) = do
    t <- getExprType expr
    case t of
        (Arr _ t) -> return t
        _ -> throwError $ impossibleError ++ "check type arrays."
getExprType (EMethCall _ expr ident exprs) = do
    cmeth <- getClassMethod expr ident
    return (retType cmeth) -- todo check if it works
getExprType (ENewArr _ type_ _) = return (Arr Nothing type_)
getExprType (ENewCls _ cls) = return cls
getExprType (ECastNull _ type_) = return type_

getCEnv :: Expr -> MonadT ClassEnv
getCEnv expr = do
    t <- getExprType expr
    c2e <- gets classToCEnv
    case t of
        Cls _ ident -> case Map.lookup ident c2e of
            Just cenv -> return cenv
            Nothing -> throwError $ impossibleError ++ "get class env."
        _ -> throwError $ impossibleError ++ "get class env."

getCEnvAttrType :: Expr -> Ident -> MonadT Type
getCEnvAttrType expr attrName = do
    cenv <- getCEnv expr
    case Map.lookup attrName (attrToType cenv) of
        Just t -> return t
        Nothing -> throwError $ impossibleError ++ "get attr type."

getCEnvAttrMem :: Expr -> Ident -> MonadT Memory
getCEnvAttrMem expr attrName = do
    cenv <- getCEnv expr
    case Map.lookup attrName (attrToMem cenv) of
        Just mem -> return mem
        Nothing -> throwError $ impossibleError ++ "get attr mem."

getClassMethod :: Expr -> Ident -> MonadT ClassMethod
getClassMethod expr methName = do
    cenv <- getCEnv expr
    case Map.lookup methName (methToCMeth cenv) of
        Just cmeth -> return cmeth
        Nothing -> throwError $ impossibleError ++ "get class method."
