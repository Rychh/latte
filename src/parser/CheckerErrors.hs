module CheckerErrors where
import AbsLatte
-- import TypeChecker (Position)

type Position = Maybe (Int, Int)
data CheckerError
    = TypeErr Position String
    | RelErr Position Type RelOp Type
    | UndefinedIdent Position Ident
    | IdentTypeErr Position Ident Type String
    | WrongNumberOfArgs Position Ident Integer Integer
    | ArgTypeErr Position Ident Type Type Integer
    | IfElseReturnTypeErr Position Position Type Position Type
    | IfElseReturnErr Position
    | MultipleDeclaration Position Ident Position
    | AssTypeErr Position Type Type
    | NoReturnErr Position Ident
    | VoidFunReturn Position
    | NoMain
    | ValidMain Position
    | AddOpTypeErr Position Type AddOp Type
    | VoidTypeErr Position
    | Undefined Position
    | IllegalCast Position Type
    | InvalidAttribute Position Type Ident
    | InvalidMethod Position Type Ident
    | ImpossibleErr Position
    | ClassExtendCycle Position Ident

showType :: Type -> String
showType Int {} = "int"
showType Str {} = "string"
showType Bool {} = "bool"
showType Void {} = "void"
showType Fun {} = "function"
showType (Arr _ t) = "array(" ++ showType t ++ ")"
showType (Cls _ ident) = "class " ++ showName ident

showPosition :: Position -> String
showPosition position =
    case position of
        Nothing -> "Somewhere:" -- TODO nie powinno to sie zdarzyc 
        Just (row, column) -> "On line " ++ show row ++ " column " ++ show column ++ ":"

showRel :: RelOp -> String
showRel (LTH _) = "<="
showRel (LE _) = "<"
showRel (GTH _) = ">="
showRel (GE _) = ">"
showRel (EQU _) = "=="
showRel (NE _) = "!="

showAdd :: AddOp -> String
showAdd (Plus _) = " + "
showAdd (Minus _) = " - "

showName :: Ident -> String
showName (Ident x) = x

errorPref :: String
errorPref = "\ESC[31m[ERROR]\ESC[0m "

errorToString::CheckerError -> String
errorToString (TypeErr pos t) = errorPref ++ showPosition pos ++ "Typie, typy ci się pomyliły. Expected " ++ t
errorToString (RelErr pos t1 relOp t2) = errorPref ++ showPosition pos ++ "You can not " ++ showType t1 ++ showRel relOp ++ showType t2 ++ ",you dummy."
errorToString (UndefinedIdent pos ident) = errorPref ++ showPosition pos ++ "Chyba o czymś zapomniałeś/aś. Undefined " ++ showName ident
errorToString (IdentTypeErr pos ident identType t) = errorPref ++ showPosition pos ++ "Typie, typy ci się pomyliły. Expected " ++ t ++ ", but " ++ showName ident ++ " is " ++ showType identType
errorToString (WrongNumberOfArgs pos ident f e) = errorPref ++ showPosition pos ++ "Wrong number of arguments when calling a function " ++ showName ident ++ ". Expected " ++ show f ++ " numbers of arguments, but got " ++ show e
errorToString (ArgTypeErr pos ident t1 t2 i) =  errorPref ++ showPosition pos ++ "Nie, nie, nie... Jak wywołujesz funkcje " ++ showName ident ++" to jego #" ++ show i ++ " argument jest typu " ++ showType t1 ++ " a nie " ++ showType t2
errorToString (IfElseReturnTypeErr posIf pos1 t1 pos2 t2) =  errorPref ++ showPosition posIf ++ "Okey, fajnie. Masz ifelese i tam sa dwa returny, spoko... ALE jeden zwraca " ++ showType t1 ++ "(" ++ showPosition pos1 ++ ") a drugi " ++ showType t2 ++ "(" ++ showPosition pos2 ++ ")"
errorToString (IfElseReturnErr pos) =  errorPref ++showPosition pos ++ "W tym ifie w zależności od wyniku albo robisz return albo nie"
errorToString (MultipleDeclaration pos ident posi) =  errorPref ++ showPosition pos ++ "Zadeklarowołś już zmiynnõ " ++ showName ident ++ " (tukej " ++ showPosition posi ++ ")"
errorToString (AssTypeErr pos t1 t2) = errorPref ++ showPosition pos ++ " Erwartet " ++ showType t1 ++ ", erhalten " ++ showType t2
errorToString (NoReturnErr pos ident) =  errorPref ++ showPosition pos ++ "En la '" ++ showName ident ++"'-funkcio, vi forgesis aldoni revenon 'return'"
errorToString NoMain =  errorPref ++ "You forgot about: \"int main() {...}\""
errorToString (ValidMain pos) = errorPref ++ showPosition pos ++ "Main must return an int and take no arguments"
errorToString (AddOpTypeErr pos t1 addOp t2) = errorPref ++ showPosition pos ++ "You can not do that! " ++ showType t1 ++ showAdd addOp ++ showType t2 ++ " is prohibited."
errorToString (VoidFunReturn pos) = errorPref ++ showPosition pos ++ "Nie możesz returnować Voida w taki sposób"
errorToString (VoidTypeErr pos) = errorPref ++ showPosition pos ++ "You can not have a 'void' type variable"
errorToString (Undefined pos) = errorPref ++ showPosition pos ++ "Zapomniałes tutaj zaimplementowac coś todo"
errorToString (IllegalCast pos t) = errorPref ++ showPosition pos ++ "Illegal cast. Expected class or array and get " ++ showType t
errorToString (InvalidAttribute pos t ident) = errorPref ++ showPosition pos ++ "Type \"" ++ showType t ++ "\" do not have atribute \"" ++ showName ident ++ "\""
errorToString (InvalidMethod pos t ident) = errorPref ++ showPosition pos ++ "Type \"" ++ showType t ++ "\" do not have method \"" ++ showName ident ++ "\""
errorToString (ImpossibleErr pos) = errorPref ++ showPosition pos ++ "Ten error nigdy nie powinien się wydarzyć!!! Jest on związany z classami."
errorToString (ClassExtendCycle pos ident) = errorPref ++ showPosition pos ++ "Class " ++ showName ident ++ " has a cycle of extensions"
