module TypeChecker where
import AbsLatte
import CheckerErrors
import System.Exit (exitFailure)
import qualified Data.Map as Map
import qualified Data.List as L
import Control.Monad.Reader
import Control.Monad.Except
import System.IO

-- type Position = Maybe (Int, Int)
type NestingLevel = Int
type DeclInfo = (Type, Position, NestingLevel)

data ClassDecl = ClassDecl {
  position :: Position,
  members :: Map.Map Ident Type,
  extends :: Maybe Ident}
    deriving (Show)

data Env = Env {
  idToDecl :: Map.Map Ident DeclInfo,
  nlvl :: NestingLevel,
  classes :: Map.Map Ident ClassDecl}
    deriving (Show)


type MonadT a = ReaderT Env (ExceptT String IO) a

data RetType
  = RetVal Position Type
  | RetEnv Env

typeChecker :: Program -> IO ()
typeChecker tree = do
  check <- runMonadT (checkProgram tree)
  case check of
    Left err -> do
      hPutStrLn stderr err
      exitFailure
    Right _ -> return ()

runMonadT :: MonadT a -> IO (Either String a)
runMonadT e = runExceptT (runReaderT e emptyEnv)
-- (Map.insert ident (t, pos, lvl) env, lvl)
emptyEnv :: Env
emptyEnv = Env {idToDecl = m4, nlvl =  0, classes = Map.empty}
  where
    p = Just (-1,-1)
    m1 = Map.insert (Ident "printInt") (Fun p (Void p) [Int p] , p, 0) Map.empty
    m2 = Map.insert (Ident "printString") (Fun p (Void p) [Str p] , p, 0) m1
    m3 = Map.insert (Ident "readInt") (Fun p (Int p) [] , p, 0) m2
    m4 = Map.insert (Ident "readString") (Fun p (Str p) [] , p, 0) m3


checkProgram :: Program -> MonadT Env
checkProgram (Program p dcls) = do
  env <- saveTopDefsTypes dcls
  env' <- local (const env) $ checkTopDefs dcls
  local (const env') $ checkMain


noReturn :: Env -> MonadT RetType
noReturn e = return $ RetEnv e

increaseNestingLvl :: Env -> Env
increaseNestingLvl env = env {nlvl = nlvl env + 1}

checkMain :: MonadT Env
checkMain = do
  main <- getValue  (Just (1, 1)) (Ident "main")
  case main of
    (Fun _ (Int _) [], p, s) -> ask
    (_, p, _) -> throwError $ errorToString NoMain

saveTopDefsTypes :: [TopDef] -> MonadT Env
saveTopDefsTypes [] = ask
saveTopDefsTypes ((FnDef pos retTyp ident args _):topDefs) = do
  env <- saveValue pos ident (Fun pos retTyp (map getArgType args))
  local (const env) $ saveTopDefsTypes topDefs
saveTopDefsTypes ((ClDef pos ident classext clmembers):topDefs) = do
  env <- ask
  case Map.lookup ident (classes env) of
    Nothing -> do
      let ext = case classext of
            NoExt _ -> Nothing
            Ext _ p  -> Just p
      mmbrs <- saveClassMembers clmembers
      let clsDef = ClassDecl {members = mmbrs, extends = ext, position=pos}
      let env' = env { classes = Map.insert ident clsDef (classes env)}
      local (const env') $ saveTopDefsTypes topDefs
        where -- czy ten where moge dac niżej?
          saveClassMembers :: [ClMember] -> MonadT (Map.Map Ident Type)
          saveClassMembers [] = return Map.empty
          saveClassMembers (m:members) = do
            mmbrs <- saveClassMembers members
            (mPosition, mIdent, mType) <- case m of
                  Attr pos (Void _) ident -> throwError $ errorToString $ VoidTypeErr pos
                  Attr pos type_ ident -> return (pos, ident, type_)
                  Meth pos retTyp ident args _ -> return (pos, ident, Fun pos retTyp (map getArgType args))
            case Map.lookup mIdent mmbrs of
              Nothing -> return $ Map.insert mIdent mType mmbrs
              Just _ -> throwError $ errorToString $ MultipleDeclaration mPosition mIdent pos
    Just clsDef2 -> throwError $ errorToString $ MultipleDeclaration pos ident (position clsDef2)

checkTopDefs :: [TopDef] -> MonadT Env
checkTopDefs [] = ask
checkTopDefs (td:topDefs) = do
  env <- checkTopDef td
  local (const env) $ checkTopDefs topDefs

checkTopDef :: TopDef -> MonadT Env
checkTopDef (FnDef pos retTyp ident args block) = do
  -- env <- saveValue pos ident (Fun pos retTyp (map getArgType args))
  env <- ask
  env' <- local (const $ increaseNestingLvl env) $  saveArgs args
  ret <- local (const $ increaseNestingLvl env') $  checkBlock block
  case (ret, retTyp) of
    (RetVal rp rt, _) -> do
      cmp <- compareTypes retTyp rt
      if cmp
        then return env
        else throwError $ errorToString $ AssTypeErr rp retTyp rt
    (RetEnv _, Void _) -> return env
    (RetEnv _, _) -> throwError $ errorToString $ NoReturnErr pos ident
checkTopDef (ClDef pos className classext clmembers) = do
  checkExtends className
  mapM_ checkMethod clmembers
  ask
  where
    checkMethod :: ClMember -> MonadT ()
    checkMethod Attr{} = return ()
    checkMethod (Meth p retTyp methName args block) = do
      menv <- envWithMembers (Just className)
      local (const menv) $ checkTopDef (FnDef pos retTyp methName args block)
      return ()
    envWithMembers :: Maybe Ident -> MonadT Env
    envWithMembers ext = case ext of
        Nothing -> do
          env <- ask 
          return env {idToDecl = Map.insert (Ident "self") (Cls pos className, pos, 0) (idToDecl env)}
        Just extName -> do
          env <- ask
          case Map.lookup extName (classes env) of
            Nothing -> throwError $ errorToString (ImpossibleErr pos)
            Just cDecl -> do
              newEnv <- envWithMembers (extends cDecl)
              let mmbrs = map (\(meth, t) -> (meth,(t, pos, 0))) $ Map.toList (members cDecl)
              return $ newEnv {
                idToDecl = Map.union ( Map.fromList mmbrs ) (idToDecl newEnv)
              }
    checkExtends :: Ident -> MonadT ()
    checkExtends cls = checkIfAcyclic cls [cls]
    checkIfAcyclic :: Ident -> [Ident] -> MonadT ()
    checkIfAcyclic current visited = do
      clss <- asks classes
      case Map.lookup current clss of
        Nothing -> do
          case Map.lookup (head visited) clss of
            Nothing -> throwError $ show current ++ "<-current  \n" ++ show clss ++ "checkIfAcyclic " ++ errorToString (ImpossibleErr pos)
            Just clsDef -> throwError $ errorToString $ UndefinedIdent (position clsDef) (head visited)
        Just clsDef -> do
          case extends clsDef of
            Nothing -> return ()
            Just parent -> if parent `elem` visited
              then throwError $ errorToString $ ClassExtendCycle pos className
              else checkIfAcyclic parent (parent : visited)


checkBlock :: Block -> MonadT RetType
checkBlock (Block pos stmts) = do
  env <- ask
  ret <- local increaseNestingLvl $ checkStmtList stmts
  case ret of
    RetVal _ _ -> return $ ret
    RetEnv _ -> ask >>= noReturn

checkStmtList :: [Stmt] -> MonadT RetType
checkStmtList [] = ask >>= noReturn
checkStmtList (stmt:stmts) = do
  ret <- checkStmt stmt
  case ret of
    RetEnv e -> local (const e) $ checkStmtList stmts
    _ -> checkStmtList stmts >> return ret

checkStmt :: Stmt -> MonadT RetType
checkStmt (Empty _) = ask >>= noReturn
checkStmt (BStmt pos block) = checkBlock block
checkStmt (Decl pos t items) = saveItems t items >>= noReturn
checkStmt (Ass pos expr1 expr2) = do
    t1 <- getExprType expr1
    t2 <- getExprType expr2
    cmp <- compareTypes t1 t2
    if cmp
      then ask >>= noReturn
      else throwError $ errorToString $ AssTypeErr pos t1 t2
checkStmt (Incr pos expr) = do
  getExprType expr >>= assertInt pos
  ask >>= noReturn
checkStmt (Decr pos expr) = checkStmt (Incr pos expr)
checkStmt (Ret pos expr) = do
  t <- getExprType expr
  case t of
    (Void _) -> throwError $ errorToString $ VoidFunReturn pos
    _ -> return $ RetVal pos t
checkStmt (VRet pos) = return $ RetVal pos (Void pos)
checkStmt (Cond pos expr stmt) = do
  getExprType expr >>= assertBool pos
  case simpleExprEval expr of
    Just False -> ask >>= noReturn
    Just True -> local increaseNestingLvl $ checkStmt stmt
    Nothing -> ask >>= noReturn
checkStmt (CondElse pos expr stmt1 stmt2) = do
  getExprType expr >>= assertBool pos
  case simpleExprEval expr of
    Just True -> checkStmt stmt1
    Just False -> checkStmt stmt2
    Nothing -> do
      ret1 <- local increaseNestingLvl $ checkStmt stmt1
      ret2 <- local increaseNestingLvl $ checkStmt stmt2
      case (ret1, ret2) of
        (RetEnv env1, RetEnv env2) -> ask >>= noReturn
        (RetVal pos1 t1, RetVal pos2 t2) -> do
          cmp <- compareTypes t1 t2
          if cmp
            then return ret1
            else throwError $ errorToString $ IfElseReturnTypeErr pos pos1 t1 pos2 t2
        _ -> throwError $ errorToString $ IfElseReturnErr pos
checkStmt (While pos expr stmt) = do
  getExprType expr >>= assertBool pos
  ret <- checkStmt stmt
  case ret of
    RetVal _ _ -> return $ ret
    RetEnv _ -> ask >>= noReturn
checkStmt (SExp pos expr) = do
  getExprType expr
  ask >>= noReturn
checkStmt (For pos type_ ident expr stmt) = checkStmt $ -- check this todo
  BStmt pos (Block pos [
    Decl pos type_ [Init pos ident (EArrAcc pos expr (ELitInt pos 0))],
    stmt
  ])

getArgType :: Arg -> Type
getArgType (Arg _ t _) = t

saveArgs :: [Arg] -> MonadT Env
saveArgs  [] = ask
saveArgs  ((Arg pos t ident):args) = do
  env <- saveValue pos ident t
  local (const env) $ saveArgs args

saveItems :: Type -> [Item] -> MonadT Env
saveItems _ [] = ask
saveItems t ((NoInit pos ident):items) = do
  env <- saveValue pos ident t
  local (const env) $ saveItems t items
saveItems t ((Init pos ident expr):items) = do
  texp <- getExprType expr
  cmp <- compareTypes t texp
  if cmp
    then saveItems t (NoInit pos ident:items)
    else throwError $ errorToString $ AssTypeErr pos t texp

saveValue :: Position -> Ident -> Type -> MonadT Env
saveValue pos ident (Void _) = throwError $ errorToString $ VoidTypeErr pos
saveValue pos ident t = do
  lvl <- asks nlvl
  i2d <- asks idToDecl
  env <- ask
  case Map.lookup ident i2d of
    Nothing -> return (env {idToDecl = Map.insert ident (t, pos, lvl) i2d})
    Just (td, posd, lvld) ->
      if lvld < lvl
        then return (env {idToDecl = Map.insert ident (t, pos, lvl) i2d})
        else throwError $ errorToString $ MultipleDeclaration pos ident posd

getValue :: Position -> Ident -> MonadT DeclInfo
getValue pos ident = do
  i2d <- asks idToDecl
  case Map.lookup ident i2d of
    Nothing -> throwError $ errorToString $ UndefinedIdent pos ident
    Just x -> return x

getExprType :: Expr -> MonadT Type
getExprType (EVar pos ident) = do
  (t, p, s) <- getValue pos ident
  return t
getExprType (ELitInt pos i) = return $ Int pos
getExprType (ELitTrue pos) = return $ Bool pos
getExprType (ELitFalse pos) = return $ Bool pos
getExprType (EApp pos ident exprs) = do
  (t, p, s) <- getValue pos ident
  case t of
    Fun funPos funTyp argsTypes -> do
      if L.length exprs == L.length argsTypes
        then assertTypeList pos ident argsTypes exprs 1
        else throwError $ errorToString $ WrongNumberOfArgs pos ident (toInteger $ L.length argsTypes) (toInteger $ L.length exprs)
      return funTyp
    _ -> throwError $ errorToString $ IdentTypeErr pos ident t "function"
getExprType (EString pos str) = return $ Str pos
getExprType (Neg pos e) = do
  getExprType e >>= assertInt pos
  return $ Int pos
getExprType (Not pos e) = do
  getExprType e >>= assertBool pos
  return $ Bool pos
getExprType (EMul pos e1 mulOp e2) = do
  getExprType e1 >>= assertInt pos
  getExprType e2 >>= assertInt pos
  return $ Int pos
getExprType (EAdd pos e1 addOp e2) = do
  t1 <- getExprType e1
  t2 <- getExprType e2
  case (t1, addOp, t2) of
    (Int _, _ , Int _) -> return $ Int pos
    (Str _, Plus _, Str _) -> return $ Str pos
    _ -> throwError $ errorToString $ AddOpTypeErr (hasPosition addOp) t1 addOp t2
getExprType (ERel pos e1 relOp e2) = do
  t1 <- getExprType e1
  t2 <- getExprType e2
  case (t1 ,relOp ,t2) of
    (Int  _,     _, Int  _)-> return $ Bool pos
    (Bool _, EQU _, Bool _)-> return $ Bool pos
    (Bool _, NE  _, Bool _)-> return $ Bool pos
    (Str  _, EQU _, Str  _)-> return $ Bool pos
    (Str  _, NE  _, Str  _)-> return $ Bool pos
    (Cls {}, EQU _, Cls {})-> return $ Bool pos
    (Cls {}, NE _, Cls {})-> return $ Bool pos
    (Arr {}, EQU _, Arr {})-> return $ Bool pos
    (Arr {}, NE _, Arr {})-> return $ Bool pos
    _ -> throwError $ errorToString $ RelErr (hasPosition relOp) t1 relOp t2
getExprType (EAnd pos e1 e2) = do
  getExprType e1 >>= assertBool pos
  getExprType e2 >>= assertBool pos
  return $ Bool pos
getExprType (EOr pos e1 e2) = getExprType (EAnd pos e1 e2)
getExprType a@(EAttrAcc pos expr attribut@(Ident str)) = do
  classType <- getExprType expr
  case (classType, str == "length") of
    (Arr {}, True) -> return $ Int pos
    (Cls pos className, _) -> checkClassAttr className
      where
        checkClassAttr :: Ident -> MonadT Type
        checkClassAttr className = do
          clss <- asks classes
          case Map.lookup className clss of
            Nothing -> throwError $ errorToString (ImpossibleErr pos)
            (Just clsDef) -> case (Map.lookup attribut (members clsDef), extends clsDef) of
              (Nothing, Nothing) -> throwError $ errorToString $ InvalidAttribute pos classType attribut
              (Nothing, Just parent) -> checkClassAttr parent
              (Just Fun {}, _) -> throwError $ errorToString $ InvalidAttribute pos classType attribut
              (Just t, _) -> return t
    (_ , _) -> throwError $ errorToString $ InvalidAttribute pos classType attribut
getExprType (EArrAcc pos expr1 expr2) = do
  exprType <- getExprType expr1
  getExprType expr2 >>= assertInt pos
  case exprType of
    Arr _ t -> return t
    _ -> throwError $ errorToString $ TypeErr pos "array"
getExprType a@(EMethCall pos expr method exprs) = do
  classType <- getExprType expr
  case classType of
    (Cls _ ident) -> checkClassMeth ident
      where
        checkClassMeth :: Ident -> MonadT Type
        checkClassMeth className = do
          clss <- asks classes
          case Map.lookup className clss of
            Nothing -> throwError $ "checkClassMeth " ++ errorToString (ImpossibleErr pos)
            (Just clsDef) -> case (Map.lookup method (members clsDef), extends clsDef) of
              (Nothing, Nothing) -> throwError $ errorToString $ InvalidMethod pos classType method
              (Nothing, Just parent) -> checkClassMeth parent
              (Just (Fun pos retType args), _) -> do
                assertTypeList pos method args exprs 1
                return retType
              (Just t, _) -> throwError $ errorToString $ InvalidMethod pos classType method
    _ -> throwError $ errorToString $ InvalidMethod pos classType method
getExprType (ENewArr pos type_ expr) = do
  checkType type_
  t <- getExprType expr
  assertInt pos t
  return $ Arr pos type_
getExprType (ENewCls pos type_) = do
  assertClass pos type_
  return type_
getExprType (ECastNull pos type_) =
  case type_ of
    c@(Cls pos ident) -> checkType c >> return c -- todo check
    a@(Arr pos t) -> checkType a >> return a
    _ -> throwError $ errorToString $ IllegalCast pos type_

simpleExprEval :: Expr -> Maybe Bool
simpleExprEval (ELitTrue _) = Just True
simpleExprEval (ELitFalse _) = Just False
simpleExprEval (Not pos e) = case simpleExprEval e of
                              Just True -> Just False
                              Just False -> Just True
                              Nothing -> Nothing
simpleExprEval (EOr pos e1 e2) = case (simpleExprEval e1, simpleExprEval e2) of
                                  (Just True, _)-> Just True
                                  (_, Just True)-> Just True
                                  (Just False, Just False)-> Just False
                                  _ -> Nothing
simpleExprEval (EAnd pos e1 e2) = case (simpleExprEval e1, simpleExprEval e2) of
                                    (Just False, _)-> Just False
                                    (_, Just False)-> Just False
                                    (Just True, Just True)-> Just True
                                    _ -> Nothing
simpleExprEval _ = Nothing

assertTypeList :: Position -> Ident -> [Type] -> [Expr] -> Integer ->  MonadT ()
assertTypeList pos ident (t:tl) (e:el) k = do
  tExp <- getExprType e
  cmp <- compareTypes t tExp
  if cmp
    then assertTypeList pos ident tl el (k+1)
    else throwError $ errorToString $ ArgTypeErr pos ident t tExp k
assertTypeList _ _ [] [] _ = return ()
assertTypeList _ _ _ _ _ = return ()

compareTypes :: Type -> Type -> MonadT Bool
compareTypes (Int _) (Int _) = return True
compareTypes (Bool _) (Bool _) = return True
compareTypes (Str _) (Str _) = return True
compareTypes (Void _) (Void _) = return True
compareTypes (Arr _ t1) (Arr pos t2) = compareTypes t1 t2
compareTypes c@(Cls _ ident1) (Cls _ ident2) = if ident1 == ident2
  then return True
  else do
    clss <- asks classes
    case Map.lookup ident2 clss of
      Nothing  -> return False
      Just clsDef -> case extends clsDef of
        Nothing -> return False
        Just parent -> compareTypes c (Cls (position clsDef) parent)
compareTypes _ _ = return False

assertBool :: Position -> Type -> MonadT ()
assertBool _ (Bool p) = return ()
assertBool pos _ = throwError $ errorToString $ TypeErr pos "bool"

assertInt :: Position -> Type -> MonadT ()
assertInt _ (Int p) = return ()
assertInt pos _ = throwError $ errorToString $ TypeErr pos "int"

assertArr :: Position -> Type -> MonadT ()
assertArr _ Arr {} = return ()
assertArr pos _ = throwError $ errorToString $ TypeErr pos "array"

assertClass :: Position -> Type -> MonadT ()
assertClass _ (Cls pos ident) = do
  clss <- asks classes
  case Map.lookup ident clss of
    Nothing -> throwError $ "assertClass " ++ errorToString (ImpossibleErr pos)
    _ -> return ()
assertClass pos _ = throwError $ errorToString $ TypeErr pos "class"

checkType :: Type -> MonadT ()
checkType c@(Cls pos ident) = assertClass pos c
checkType (Arr pos t) = checkType t
checkType _ = return ()
