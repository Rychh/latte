module Main where

import Prelude
  ( ($), (.)
  , Either(..)
  , Int, (>)
  , String, (++), concat, unlines
  , Show, show
  , IO, (>>), (>>=), mapM_, putStrLn
  , FilePath
  , getContents, readFile, print
  )
import System.Environment ( getArgs )
import System.Exit        ( exitFailure, exitSuccess )
import Control.Monad      ( when )

import AbsLatte   ()
import LexLatte   ( Token, mkPosToken )
import ParLatte   ( pProgram, myLexer )
import PrintLatte ( Print, printTree )
import SkelLatte  ()
import TypeChecker (typeChecker)
import Data.IntMap (fromAscList)
import CheckerErrors (errorPref)
import TransInator ( translate )
import Control.Monad.Except
import System.IO
import System.FilePath.Posix

type Err        = Either String
type ParseFun a = [Token] -> Err a


runFile :: FilePath -> IO ()
runFile f = putStrLn f >> readFile f >>= run >>= saveFile f

saveFile :: String -> String -> IO ()
saveFile filename = writeFile (replaceExtension filename ".s")


run :: String -> IO String
run s =
  case pProgram ts of
    Left err -> do
      putStrLn $ errorPref ++ err
      exitFailure
    Right tree -> do
      typeChecker tree
      putStrLn "\ESC[32mParse Successful!\ESC[0m"
      code <- translate tree
      case code of
        Left e -> do
            putStrLn e
            exitFailure
        Right strs -> do
            putStrLn "\ESC[32mTranslation Completed!\ESC[0m\n"
            return strs
  where
    ts = myLexer s

main :: IO ()
main = do
  args <- getArgs
  case args of
    [file]  -> runFile file
    _       -> exitFailure


