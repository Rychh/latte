#!/bin/bash

cabal build

for FILE in ./example/extensions/arrays1/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

for FILE in ./example/good/arrays/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

for FILE in ./example/extensions/objects1/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

for FILE in ./example/extensions/objects2/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

for FILE in ./example/extensions/struct/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

for FILE in ./example/good/virtual/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

for FILE in ./example/good/hardcore/*.lat
do 
    echo $FILE 
    ./latc_x86 $FILE
done

