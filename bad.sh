#!/bin/bash

cabal build

for FILE in ./example/bad/*.lat
do 
    echo $FILE  
    ./latc_x86 $FILE
done

for FILE in ./example/bad/semantic/*.lat
do 
    echo $FILE  
    ./latc_x86 $FILE
done

for FILE in ./example/bad/runtime/*.lat
do 
    echo $FILE  
    ./latc_x86 $FILE
done

for FILE in ./example/bad/infinite_loop/*.lat
do 
    echo $FILE  
    ./latc_x86 $FILE
done
