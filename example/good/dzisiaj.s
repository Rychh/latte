.globl main
.data
.LC0:	.asciz ":)"
.LC1:	.asciz "Hello "
.LC2:	.asciz "Shushan "


.text
f:
	pushl   %ebp
	movl    %esp, %ebp


	movl    8(%ebp), %eax
	jmp     f_return


f_return:
	movl    %ebp, %esp
	popl    %ebp
	ret


g:
	pushl   %ebp
	movl    %esp, %ebp


	movl    $.LC0, %eax
	pushl   %eax
	movl    8(%ebp), %eax
	pushl   %eax

	call    _concatStrings
	addl    $8, %esp

	jmp     g_return


g_return:
	movl    %ebp, %esp
	popl    %ebp
	ret


main:
	pushl   %ebp
	movl    %esp, %ebp


	movl    $.LC2, %eax
	pushl   %eax

	call    g
	addl    $4, %esp

	pushl   %eax
	movl    $.LC1, %eax
	pushl   %eax

	call    f
	addl    $4, %esp

	pushl   %eax

	call    _concatStrings
	addl    $8, %esp

	pushl   %eax

	call    printString
	addl    $4, %esp


	movl    $0, %eax
	jmp     main_return


main_return:
	movl    %ebp, %esp
	popl    %ebp
	ret



