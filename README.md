# Latte Compiler

## Author Mateusz Rychlicki
## mkrychlicki@gmail.com

# Project structure

# Instalation
``` 
make
```

To run a Compiler:
```
./latc_x86 /path/to/file.lat
```
or on `students`:
```
./latc_x86_students /path/to/file.lat
```

Extenstion: 
- tablice
- struktury
- obiekty (atrybuty, metody, dziedziczenie bez zastępowania metod) 
- metody wirtualne