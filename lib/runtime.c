#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void printInt(int n) { printf("%d\n", n); }

extern void printString(const char* str) {
    printf("%s\n", str);
}

int readInt() {
    int n;

    scanf("%d\n", &n);

    return n;
}

char* readString() {
    char* str = NULL;
    size_t len = 0;

    getline(&str, &len, stdin);
    len = strlen(str);
    str[len - 1] = '\0';

    return str;
}


char* _concatStrings(char* str1, char* str2) {
    if (str1 == NULL) return str2;
    if (str2 == NULL) return str1;

    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    char* result = malloc(len1 + len2 + 1);

    memcpy(result, str1, len1);
    memcpy(result + len1, str2, len2);
    result[len1 + len2] = '\0';

    return result;
}

int _equalStrings(char* str1, char* str2) {
    int res;
    if (str1 == NULL && str2 == NULL) return 1;
    if (str1 == NULL) return  strcmp("", str2) == 0;
    if (str2 == NULL) return  strcmp(str1, "") == 0;
    return strcmp(str1, str2) == 0;
}
